<?php
/*
Template Name: Líderes Sedes
*/
get_header();

$taxonomy_terms = get_terms( 'lideres_levels' );

foreach($taxonomy_terms as $i => $taxonomy_term):
	$lideres_query = new WP_Query(array('post_type' => 'lideres', 'lideres_levels' => $taxonomy_term->slug, 'order' => 'ASC', 'orderby' => 'name'));
	$lideres = (count($lideres_query->posts)) ? $lideres_query->posts : array();
	unset($lideres_query);
?> 

<!-- *************************** -->
<!-- ********* Content ********* -->
<!-- *************************** -->

<div class="container">
	<section class="row iasd-author-list">
		<article class="col-md-12 xs-landscape">
			<header <?php if($i != 0): echo 'class="mar-top-50"'; endif; ?> >
				<h1 class="iasd-main-title"><?php echo $taxonomy_term->name; ?></span><span class="visible-xs">.</span></h1>
			</header>

			<?php
				foreach($lideres as $lider):
			?>
			<span class="xs-landscape">
				<div class="col-sm-4 col-md-3">
					<a href="<?php echo get_permalink($lider->ID); ?>">
						<figure class="img-circle">
							<?php
								add_filter('no_default_image', '__return_true');
								$lider_thumb_id = get_post_thumbnail_id($lider->ID);
								$lider_thumb_url = wp_get_attachment_image_src( $lider_thumb_id, 'thumb_124x124' );
								remove_filter('no_default_image', '__return_true');
							?>
							<div class="img-holder" style="background: url(<?php echo $lider_thumb_url[0] ?>);" >
								<div class="img-gradient"></div>
							</div>
						</figure>
						<h3><?php echo $lider->post_title; ?></h3>
						<h4><?php echo get_post_meta($lider->ID, 'iasd_cargo', true); ?></h4>
					</a>
				</div>
			</span>

			<?php endforeach; ?>

			<?php 
				if($i != 0){
				?> <div class="clearfix"></div> <?php
				}
			?>

		</article>
	</section>			
</div>
<?php 
endforeach;
?>
<!-- *************************** -->
<!-- ******* End Content ******* -->
<!-- *************************** -->

<?php get_footer(); ?>