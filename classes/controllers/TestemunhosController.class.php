<?php
/*
Plugin Name: Testemunhos do Portal Adventista - ASN
Description: Inclui o posttype testemulhos com link de share do video. Requer o plugin <b>Video Thumbnails</b> para gerar thumbnails e <b>Fluid Video Embed</b> para exibir corretamente.
Author: Nextt - Sidney Ferreira
Version: 0.1
Text Domain: testemunhos
*/


class TestemunhosController {

	static $text_domain     = 'testemunhos';
	static $post_type_name  = 'testemunhos';
	static $field_name      = 'testemunhos_video_share_url_field';
	static $error_meta      = 'testemunhos_error';

	public static function Init() {
		self::RegisterType();
		self::RegisterTaxonomies();
	}

	public static function RegisterTaxonomies() {

	}

	public static function TypeLabels() {
		$labels = array(
			'name'              => __('Testemunhos', 'thema_deptos'),
			'singular_name'     => __('testemunho', 'thema_deptos'),
			'add_new'           => __('Adicionar novo', 'thema_deptos'),
			'add_new_item'      => __('Adicionar novo testemunho', 'thema_deptos'),
			'edit_item'         => __('Editar testemunho', 'thema_deptos'),
			'new_item'          => __('Novo testemunho', 'thema_deptos'),
			'view_item'         => __('Visualizar testemunho', 'thema_deptos'),
			'search_items'      => __('Buscar testemunhos', 'thema_deptos')
		);
		return $labels;
	}

	public static function TypeIcon() {
		return '';
	}

	public static function TypeSlug() {
		return array('slug' => 'testemunho');
	}

	public static function TypeArguments() {
		$args = array(
			'labels' => self::TypeLabels(),
			'public' => true,
			'rewrite' => self::TypeSlug(),
			'capability_type' => 'post',
			'hierarchical' => false,
			'supports' => array('title','thumbnail', 'editor','excerpt','comments'),
			'has_archive' => 'testemunhos'
		);
		return $args;
	}

	public static function RegisterType() {
		register_post_type( self::$post_type_name , self::TypeArguments() );

	}

	public static function WPAudioPostTypes($post_types = false) {
		return array(self::$post_type_name);
	}

	public static function Nonce() {
		return wp_nonce_field( plugin_basename( __FILE__ ), self::$post_type_name );
	}

	public static function SavePost($post_id) {
		$post = get_post($post_id);
		if($post->post_type == self::$post_type_name ) {
			if(!wp_is_post_revision( $post_id ) && !wp_is_post_autosave( $post_id ) && count($_POST)) {
				if(isset($_POST[self::$field_name])) {
					$video_share_url_unfiltered = trim($_POST[self::$field_name]);

					$video_share_url = filter_var($video_share_url_unfiltered, FILTER_VALIDATE_URL);

					if($video_share_url) {
						update_post_meta($post_id, self::$field_name, $video_share_url);

						remove_action( 'save_post', array(__CLASS__, 'SavePost') );

						wp_update_post($post);
						if(function_exists('get_video_thumbnail')) {
							delete_post_meta( $post_id, '_video_thumbnail', true );
							get_video_thumbnail($post_id, self::$field_name);
						}

						add_action( 'save_post', array(__CLASS__, 'SavePost') );
						set_post_format($post_id, 'video');
					} else if($video_share_url_unfiltered) {
						$current_user = wp_get_current_user();
						update_user_meta( $current_user->ID, self::$error_meta, __('O endereço usado não é válido. Preencha o campo de acordo com as instruções.', 'thema_deptos') );
						delete_post_meta($post_id, self::$field_name);
					}
					$audio_url = get_post_meta($post_id, 'wp_audio_url');
					if($audio_url) {
						set_post_format($post_id, 'audio');
					}
					if(!$video_share_url && !$audio_url) {
						if(get_post_thumbnail_id($post_id)) {
							set_post_format($post_id, 'image');
						} else {
							set_post_format($post_id, 'standard');
						}
					}
				}
			}
		}
	}
	//META BOX
	public static function AddMetaBox() {
		add_meta_box( self::$field_name,
			__('Endereço do Video', 'thema_deptos'),
			array(__CLASS__, 'AddMetaBoxVideoUrl'),
			self::$post_type_name,
			'normal',
			'high'
		);
	}

	public static function AddMetaBoxVideoUrl( $post ) {
		self::Nonce();

		$video_share_url = get_post_meta($post->ID, self::$field_name, true);
		echo '<textarea class="attachmentlinks" id="' . self::$field_name . '" name="'.self::$field_name . '">'.$video_share_url.'</textarea><br />';
		echo '<label for="'.self::$field_name . '">'. __('Por favor, preencha o campo acima com o endereço do video. Para isso, acesse o video, no YouTube ou Vimeo, clique em SHARE (Compartilhar) e copie a URL exibida.', 'thema_deptos').'</label> ';
	}

	public static function AdminNotices() {
		$current_user = wp_get_current_user();
		$error = get_user_meta($current_user->ID, self::$error_meta, true);
		if(!$error)
			return false;
		$msg = '
		<div class="error">
			<p>'.$error.'</p>
		</div>
';
		echo $msg;
		delete_user_meta( $current_user->ID, self::$error_meta );
	}
}

add_action( 'init', array('TestemunhosController', 'Init') );
add_action( 'add_meta_boxes', array('TestemunhosController', 'AddMetaBox') );
add_action( 'save_post', array('TestemunhosController', 'SavePost') );
add_action( 'admin_notices', array('TestemunhosController', 'AdminNotices') );

